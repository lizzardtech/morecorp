<div class="col-sm-9 col-sm-offset-3 col-md-8 main" style="float: initial;">
    <div class="row placeholders">
        <div class="col-md-12">
            <h2 class="text-center">Dashboard</h2>
        </div>
        <br>
        <div class="col-xs-6 col-sm-4 placeholder">
            <div class="text-center">
                <img src="data:image/gif;base64,R0lGODlhAQABAIABAAJ12AAAACwAAAAAAQABAAACAkQBADs="
                     width="200"
                     height="200"
                     class="img-fluid rounded-circle"
                     alt=""
                     style="border-radius: 100px;">

                <h4>Total Products</h4>
                <a href="{{ route('products.view') }}">
                    <h1>{{ $products }}</h1><span class="text-muted">Units</span>
                </a>
            </div>
        </div>
        <div class="col-xs-6 col-sm-4 placeholder">
            <div class="text-center">
                <img src="data:image/gif;base64,R0lGODlhAQABAIABAAJ12AAAACwAAAAAAQABAAACAkQBADs="
                     width="200"
                     height="200"
                     class="img-fluid rounded-circle"
                     alt=""
                     style="border-radius: 100px;">

                <h4>Bidded Products</h4>
                <a href="{{ route('products.view') }}">
                    <h1>{{ $biddedProducts }}</h1><span class="text-muted">Units</span>
                </a>
            </div>
        </div>
        <div class="col-xs-6 col-sm-4 placeholder">
            <div class="text-center">
                <img src="data:image/gif;base64,R0lGODlhAQABAIABAAJ12AAAACwAAAAAAQABAAACAkQBADs="
                     width="200"
                     height="200"
                     class="img-fluid rounded-circle"
                     alt=""
                     style="border-radius: 100px;">

                <h4>Total Biddings</h4>
                <a href="{{ route('products.view') }}">
                    <h1>{{ $bids }}</h1><span class="text-muted">Units</span>
                </a>
            </div>
        </div>
    </div>
</div>