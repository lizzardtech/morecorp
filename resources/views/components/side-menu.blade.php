<div class="col-sm-3 col-md-2 sidebar" style="background-color: #264556; height:100%;">
    <ul class="nav nav-sidebar">
        <br>
        <div class="text-center">
            <a href="/"><li class="glyphicon glyphicon-home" style="font-size: 30px;"></li></a>
        </div>
        <br>
        <li class="active"><a href="{{ route('products.view') }}" class="btn btn-info btn-lg">View All Products</a></li>
        <br>
        <li>
            <a href="javascript:void(0)" class="btn btn-info btn-lg" data-toggle="modal" data-target="#productModal">Add
                Product</a>
        </li>
        <br>
    </ul>
    <ul class="nav nav-sidebar">
        <li>
            <a href="{{ route('logout') }}" class="btn btn-info btn-lg"
               onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                Logout
            </a>
        </li>
        <li><br></li>
    </ul>
    <br>
</div>

@include('modals.product_add')
