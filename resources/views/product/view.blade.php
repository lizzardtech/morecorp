@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <div class="row" style="height: -webkit-fill-available">
            <div style="margin-top: -22px;">
                @include('components.side-menu')
            </div>

            @foreach($products as $product)
                <div class="col-md-9">
                    @if(\Illuminate\Support\Facades\Session::has('flash_message'))
                        <div class="alert alert-success">
                            {{ \Illuminate\Support\Facades\Session::get('flash_message') }}
                        </div>
                    @endif
                    <h2 class="text-center">Product Details</h2>
                    <br>
                    <div class="col-md-12">
                        <div class="col-md-2 text-right"><strong>SKU</strong></div>
                        <div class="col-md-10 text-left">{{ $product->sku }}</div>
                        <div class="col-md-12 clearfix">
                            <hr>
                        </div>

                        <div class="col-md-2 text-right"><strong>NAME</strong></div>
                        <div class="col-md-10 text-left">{{ $product->name }}</div>
                        <div class="col-md-12 clearfix">
                            <hr>
                        </div>

                        <div class="col-md-2 text-right"><strong>DESCRIPTION</strong></div>
                        <div class="col-md-10 text-left">{{ $product->description }}</div>
                        <div class="col-md-12 clearfix">
                            <hr>
                        </div>

                        <div class="col-md-2 text-right"><strong>PRICE</strong></div>
                        <div class="col-md-10 text-left">R {{ $product->price }}</div>
                        <div class="col-md-12 clearfix">
                            <hr>
                        </div>
                        <div class="col-md-12">
                            <br>
                            <div class="pull-right">
                                <a href="javascript:void(0)" data-toggle="modal" data-target="#productBidModal"
                                   class="btn btn-primary btn-primary">Place Bid</a>
                                <a href="javascript:void(0)" data-toggle="modal" data-target="#productUpdateModal"
                                   class="btn btn-primary btn-warning">Update</a>
                                <a href="/product/delete/{{ $product->uuid }}" class="btn btn-danger ">Delete</a>
                            </div>
                        </div>
                        <div class="col-md-12 text-left">
                            <br>
                            <h4>Product Bidding :</h4>
                            @if(!empty($biddings))
                                <div class="col-md-4"><strong>DATE</strong></div>
                                <div class="col-md-4"><strong>EMAIL</strong></div>
                                <div class="col-md-4"><strong>AMOUNT</strong></div>
                                <hr>
                                @foreach($biddings as $bidding)
                                    <div class="col-md-4">{{ $bidding['created_at'] }}</div>
                                    <div class="col-md-4">{{ $bidding['email'] }}</div>
                                    <div class="col-md-4">R {{ $bidding['amount'] }}</div>
                                @endforeach
                            @endif
                        </div>
                    </div>
                </div>
                @include('modals.product_bid')
                @include('modals.product_update')
            @endforeach

        </div>
    </div>

@endsection