@extends('layouts.default')

@include('components.main-navbar')

@section('content')

    <div class="container-fluid">
        <div class="row" style="height: -webkit-fill-available">
            <div style="margin-top: -22px;">
                @include('components.side-menu')
            </div>

            <div class="col-md-9">
                @if(\Illuminate\Support\Facades\Session::has('flash_message'))
                    <div class="alert alert-success">
                        {{ \Illuminate\Support\Facades\Session::get('flash_message') }}
                    </div>
                @endif
                <div class="col-md-12">
                    <div class="col-md-1 text-center"><strong>SKU</strong></div>
                    <div class="col-md-1 text-center"><strong>ITEM</strong></div>
                    <div class="col-md-6 text-center"><strong>DESCRIPTION</strong></div>
                    <div class="col-md-2 text-center"><strong>PRICE</strong></div>
                    <div class="col-md-2 text-right"><strong>*</strong></div>
                </div>
                <div class="col-md-12">
                    <hr>
                </div>
                @foreach($products as $product)
                    <div class="col-md-12">
                        <div class="col-md-1 text-left"> {{ $product['sku'] }} </div>
                        <div class="col-md-1 text-left"> {{ $product['name'] }} </div>
                        <div class="col-md-6"> {{ $product['description'] }} </div>
                        <div class="col-md-2 text-left">R {{ $product['price'] }} </div>
                        <div class="col-md-2 text-right">
                            <a href="product/{{ $product['uuid'] }}" class="btn btn-primary btn-xs delete">view</a>
                            <a href="/product/delete/{{ $product['uuid'] }}" class="btn btn-danger btn-xs">del</a>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <hr>
                    </div>
                @endforeach

            </div>

        </div>
    </div>

@endsection

@include('modals.product_bid')
