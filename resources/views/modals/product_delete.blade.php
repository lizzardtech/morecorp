<div id="productDeleteModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center">Are you sure you want to delete ?</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" action="{{ route('product.create') }}" method="post">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                        </div>
                        <input type="text" name="product_id" id="product_id" value=""/>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-9">
                            <button type="submit" class="btn btn-default col-md-4">Delete</button>
                            <button type="button" class="btn btn-default pull-right col-md-4" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>