<div id="productBidModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center">Place Bid</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" method="post" action="{{ route('product.bid') }}">

                    {{ csrf_field() }}
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="name">Email</label>
                        <div class="col-sm-8">
                            <div class="col-sm-8">
                                <input type="email" class="form-control" id="email" name="email"
                                       placeholder="Enter email" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="amount">Amount (R)</label>
                        <div class="col-sm-8">
                            <div class="col-sm-8">
                            <input type="text" class="form-control" id="amount" name="amount" placeholder="Enter Amount"
                                   required>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="product_id" value="{{ $product->uuid }}">
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-9">

                            <button type="submit" class="btn btn-default col-md-4">Place Bid</button>
                            <button type="button" class="btn btn-default pull-right col-md-4" data-dismiss="modal">
                                Close
                            </button>

                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>

@section('extra-scripts')

@endsection