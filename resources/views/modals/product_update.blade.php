<div id="productUpdateModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center">Update Product</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" action="{{ route('product.update') }}" method="post">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <input type="hidden" name="created_by" value="{{ \Illuminate\Support\Facades\Auth::user()->uuid }}">
                        <input type="hidden" name="id" value="{{ $product->id }}">
                        <input type="hidden" name="uuid" value="{{ $product->uuid }}">
                        <input type="hidden" name="sku" value="{{ $product->sku }}">
                        <label class="control-label col-sm-3" for="name">Product name</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="name" name="name" value="{{ $product->name }}"
                                   required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="email">Description</label>
                        <div class="col-sm-8">
                            <textarea class="form-control" id="description" name="description" required>{{ $product->description }}</textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="price">Price</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" id="price" name="price" value="{{ $product->price }}" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-9">
                            <button type="submit" class="btn btn-default col-md-4">Update</button>
                            <button type="button" class="btn btn-default pull-right col-md-4" data-dismiss="modal">
                                Close
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>