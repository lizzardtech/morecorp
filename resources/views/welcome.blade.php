@extends('layouts.default')

@section('content')

    @include('components.main-navbar')

    <div class="container-fluid">
        <div class="row">

            @include('components.side-menu')

            @include('components.status-thumbs')

        </div>
    </div>

@endsection