@extends('layouts.default')

@include('components.main-navbar')

@section('content')

    <div class="container-fluid">
        <div class="row">

            <div style="margin-top: -22px;">
                @include('components.side-menu')
            </div>

            @if(\Illuminate\Support\Facades\Session::has('flash_message'))
                <div class="alert alert-success">
                    {{ \Illuminate\Support\Facades\Session::get('flash_message') }}
                </div>
            @endif

            @include('components.status-thumbs')

            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif

        </div>
    </div>
@endsection
