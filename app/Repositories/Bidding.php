<?php

namespace App\Repositories;

use App\Models\Bidding as Model;
use Laravel5Helpers\Repositories\Repository;

class Bidding extends Repository
{
    public function getAll()
    {
        try {
            return Model::all();
        } catch (\Exception $exception) {
            return $exception->getMessage();
        }
    }

    public function getCount()
    {
        return count($this->getAll());
    }

    public function getBpCount()
    {
        return count($this->getAll());
    }

    protected function getModel()
    {
        return new Model();
    }
}
