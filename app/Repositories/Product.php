<?php

namespace App\Repositories;

use Laravel5Helpers\Repositories\Repository;
use App\Models\Product as Model;

class Product extends Repository
{
    public function getAll()
    {
        try {
            return Model::all();
        } catch (\Exception $exception) {
            return $exception->getMessage();
        }
    }

    public function getCount()
    {
        return count($this->getAll());
    }

    public function delete($id)
    {
        try {
            Model::where('uuid', $id)->delete();
        } catch (\Exception $exception) {
            return $exception->getMessage();
        }
    }

    public function update($entity, $id)
    {
        try {
            $product =  \App\Models\Product::find($id);
            $product->sku = $entity->sku;
            $product->name = $entity->name;
            $product->description = $entity->description;
            $product->price = $entity->price;
            $product->save();

        } catch (\Exxception $exception) {
            return $exception->getMessage();
        }
    }

    public function getById($id)
    {
        try {
            return Model::where('uuid', $id)->get();
        } catch (\Exception $exception) {
            return $exception->getMessage();
        }
    }

    protected function getModel()
    {
        if (empty($this->model) === TRUE) {
            $this->model = new Model();
        }

        return $this->model;
    }
}