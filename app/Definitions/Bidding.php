<?php

namespace App\Definitions;

use Laravel5Helpers\Definitions\Definition;

class Bidding extends Definition
{
    public $product_id;
    public $email;
    public $amount;

    protected function setValidators()
    {
        return $this->validators = [
            'product_id' => 'required',
            'email' => 'required',
            'amount' => 'required'
        ];
    }
}