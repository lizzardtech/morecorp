<?php

namespace App\Definitions;

use App\Utilities\UniqueCodeGen;
use Laravel5Helpers\Definitions\Definition;

class Product extends Definition
{
    public $sku ;
    public $name;
    public $description;
    public $price;
    public $created_by;

    protected function setValidators()
    {
        return $this->validators = [
            'name'        => 'required',
            'description' => 'required',
            'price'       => 'required',
            'created_by'    => 'required'
        ];
    }
}