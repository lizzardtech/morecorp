<?php

namespace App\Utilities;

use Illuminate\Support\Facades\DB;

class UniqueCodeGen
{
    public static function generate($length = 10)
    {
        $unique = '';
        do {
            for ($i = $length; $i--; $i > 0) {
                $unique .= mt_rand(0, 9);
            }
        } while (!empty(DB::table('products')->where('sku', $unique)->first(['id'])));

        return $unique;
    }
}
