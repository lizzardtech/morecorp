<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Product;
use App\Repositories\Bidding;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $params['products'] = $this->getProductRepository()->getCount();
        $params['bids']  = $this->getBiddingRepository()->getCount();
        $params['biddedProducts']  = $this->getBiddingRepository()->getBpCount();

        return view('home', $params);
    }

    private function getProductRepository()
    {
        return new Product();
    }

    private function getBiddingRepository()
    {
        return new Bidding();
    }
}
