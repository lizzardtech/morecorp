<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Bidding as Repository;
use App\Definitions\Bidding as Definition;
use Illuminate\Support\Facades\Session;
use Laravel5Helpers\Exceptions\LaravelHelpersExceptions;

class BiddingController extends Controller
{
    public function bid(Request $request)
    {
        try {
            $definition = $this->getDefinition($request);
            $params['bidding'] = $this->getRepository()->createResource($definition);
            $params['biddings'] = $this->getRepository()->getAll();
            Session::flash('flash_message', 'Bid successfully placed!');

            return redirect()->back()->with($params);
        } catch (LaravelHelpersExceptions $exception) {
            return $exception->getMessage();
        }
    }

    private function getRepository()
    {
        return new Repository;
    }

    private function getDefinition(Request $request)
    {
        return new Definition($request->all());
    }
}
