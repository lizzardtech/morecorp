<?php

namespace App\Http\Controllers;

use App\Repositories\Bidding;
use Illuminate\Http\Request;
use App\Definitions\Product as Definition;
use App\Repositories\Product as Repository;
use Illuminate\Support\Facades\Session;
use Laravel5Helpers\Exceptions\LaravelHelpersExceptions;

class ProductController extends Controller
{
    public function index()
    {
        $params['products'] = $this->getRepository()->getAll();

        return view('product.index', $params);
    }

    public function store(Request $request)
    {
        try {
            $definition = $this->getDefinition($request);
            $params['products'] = $this->getRepository()->createResource($definition);
            Session::flash('flash_message', 'Product successfully saved!');

            return redirect()->route('products.view', $params);
        } catch (LaravelHelpersExceptions $exception) {
            return $exception->getMessage();
        }
    }

    public function update(Request $request)
    {
        try {
            $definition = $this->getDefinition($request);
            $params['products'] = $this->getRepository()->update($definition, $request->get('id'));
            Session::flash('flash_message', 'Product successfully updated!');

            return redirect()->route('products.view', $params);
        } catch (LaravelHelpersExceptions $exception) {
            return $exception->getMessage();
        }
    }

    public function destroy($id)
    {
        try {
            $this->getRepository()->delete($id);
            Session::flash('flash_message', 'Product successfully deleted!');

            return redirect()->route('products.view');
        } catch (LaravelHelpersExceptions $exception) {
            return $exception->getMessage();
        }
    }

    public function show($id)
    {
        try {
            $params['products'] = $this->getRepository()->getById($id);
            $params['biddings'] = \App\Models\Bidding::all()->where('product_id', $id);

        }catch(LaravelHelpersExceptions $exception){
            return $exception->getMessage();
        }

        return view('product.view', $params);
    }

    private function getRepository()
    {
        return new Repository;
    }

    private function getDefinition(Request $request)
    {
        return new Definition($request->all());
    }
}