<?php

namespace App\Models;

use App\Utilities\UniqueCodeGen;
use Laravel5Helpers\Uuid\UuidModel;

class Product extends UuidModel
{
    protected $table = 'products';
    public $sku;
    public $timestamps = FALSE;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->sku = UniqueCodeGen::generate();
    }

    public function bidding()
    {
        return $this->hasMany('App\Models\Bidding', 'bidding_id', 'uuid');
    }
}