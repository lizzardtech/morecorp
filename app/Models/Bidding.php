<?php

namespace App\Models;

use Laravel5Helpers\Uuid\UuidModel;

class Bidding extends UuidModel
{
    protected $table = 'product_bidding';

    public function product()
    {
        $this->belongsTo('App\Models\Product', 'product_id', 'uuid');
    }
}
