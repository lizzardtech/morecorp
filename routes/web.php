<?php

Auth::routes();

Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/products', 'ProductController@index')->name('products.view');
Route::get('/product/{id}', 'ProductController@show');
Route::get('/quick-logout', 'Auth\LoginController@logout');
Route::get('/product/delete/{id}', 'ProductController@destroy');

Route::post('/product/store', 'ProductController@store')->name('product.create');
Route::post('/product/update', 'ProductController@update')->name('product.update');
Route::post('/product/bid', 'BiddingController@bid')->name('product.bid');

