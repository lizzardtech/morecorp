## About Project
MoreCorp Recruitment Excercise / Project 2017 for Developer (Web/PHP/Full-Stack) Vacancy

Created Using : LARAVEL 5.5

## Project Setup

1) Create .env file with relevant credentials (in root dir)

2) Run migrations : 
*php artisan migrate
	
3) Run database Seeders : 
*php artisan db:seed --class=UsersTableSeeder
*php artisan db:seed --class=ProductsTableSeeder
# NB: Username('admin@morecorp.co.za'); Password('admin1234');

4) Run composer	: 
*composer install

5) Install node mods	:	
*npm install
	
6) GOOD TO GO!


## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell via [taylor@laravel.com](mailto:taylor@laravel.com). All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
