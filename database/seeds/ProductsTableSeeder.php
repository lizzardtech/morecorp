<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $limit = 100;

        for ($i = 0; $i < $limit; $i++) {
            \Illuminate\Support\Facades\DB::table('products')->insert([
                'uuid' => $faker->uuid,
                'name' => $faker->word,
                'created_by' => $faker->uuid,
                'sku' => $faker->unique()->numberBetween(201701,202001),
                'price' => $faker->randomFloat(2,2),
                'description' => $faker->paragraph(1),
            ]);
        }
    }
}
